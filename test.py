import matplotlib.pyplot as plt
import numpy as np
import torch.nn.functional as F
import torch.nn as nn
import scipy.io
import ipdb
import math
import torch
import pytorch_wavelets_customize.pytorch_wavelets.dwt.lowlevel as lowlevel
from pytorch_wavelets import DWTForward, DWTInverse
import ipdb
class DWT2D(torch.nn.Module):
    def __init__(self, J=1, wave='db1', mode='zero'):
        super().__init__()
        
        # make analysis filters
        all_filters = self.make_filters()
        
        filts = all_filters[0]
        self.h0_col = filts[0]
        self.h1_col = filts[1]
        self.h0_row = filts[2]
        self.h1_row = filts[3]
        

        filts = all_filters[1]
        self.g0_col = filts[0]
        self.g1_col = filts[1]
        self.g0_row =filts[2]
        self.g1_row =filts[3]

        self.J = J
        self.mode = mode
        
    def make_filters(self):
        #self.s = torch.nn.Parameter(torch.tensor([1/math.sqrt(2)]), requires_grad=True)

        self.theta1 = torch.nn.Parameter(torch.tensor([math.pi/4]),requires_grad=True)
        self.theta2 = torch.nn.Parameter(torch.tensor([math.pi/6]),requires_grad=True)
        
        c1 = torch.cos(self.theta1)
        s1 = torch.sin(self.theta1)

        c2 = torch.cos(self.theta2)
        s2 = torch.sin(self.theta2)

        h0 = torch.cat( [c1*c2,  c2*s1, - s1*s2,  c1*s2])
        h1 = torch.cat([- c1*s2, - s1*s2, - c2*s1,  c1*c2])
        h0 = torch.flip(h0,[0])
        h1 = torch.flip(h1,[0])
        
        f0 = torch.cat( [c1*s2, - s1*s2,  c2*s1, c1*c2])
        f1 = torch.cat([c1*c2, - c2*s1, - s1*s2, - c1*s2])

        h0_col = h0.reshape((1,1,-1,1))
        h1_col = h1.reshape((1,1,-1,1))
        h0_row = h0_col.reshape((1,1,1,-1))
        h1_row = h1_col.reshape((1,1,1,-1))
        
        f0_col = f0.reshape((1,1,-1,1))
        f1_col = f1.reshape((1,1,-1,1))
        f0_row = f0_col.reshape((1,1,1,-1))
        f1_row = f1_col.reshape((1,1,1,-1))


        return (h0_col, h1_col, h0_row, h1_row), (f0_col, f1_col, f0_row, f1_row)
        
    def do_reconstruct(self, coeffs):
        """
        Args:
            coeffs (yl, yh): tuple of lowpass and bandpass coefficients, where:
              yl is a lowpass tensor of shape :math:`(N, C_{in}, H_{in}',
              W_{in}')` and yh is a list of bandpass tensors of shape
              :math:`list(N, C_{in}, 3, H_{in}'', W_{in}'')`. I.e. should match
              the format returned by DWTForward
        Returns:
            Reconstructed input of shape :math:`(N, C_{in}, H_{in}, W_{in})`
        Note:
            :math:`H_{in}', W_{in}', H_{in}'', W_{in}''` denote the correctly
            downsampled shapes of the DWT pyramid.
        Note:
            Can have None for any of the highpass scales and will treat the
            values as zeros (not in an efficient way though).
        """
        yl, yh = coeffs
        ll = yl
        mode = lowlevel.mode_to_int(self.mode)

        # Do a multilevel inverse transform
        for h in yh[::-1]:
            if h is None:
                h = torch.zeros(ll.shape[0], ll.shape[1], 3, ll.shape[-2],
                                ll.shape[-1], device=ll.device)

            # 'Unpad' added dimensions
            if ll.shape[-2] > h.shape[-2]:
                ll = ll[...,:-1,:]
            if ll.shape[-1] > h.shape[-1]:
                ll = ll[...,:-1]
            ll = lowlevel.my_reconstruct(
                ll, h, self.g0_col, self.g1_col, self.g0_row, self.g1_row, mode)
        return ll

    def do_synthesize(self,x):
        """ Forward pass of the DWT.
        Args:
            x (tensor): Input of shape :math:`(N, C_{in}, H_{in}, W_{in})`
        Returns:
            (yl, yh)
                tuple of lowpass (yl) and bandpass (yh)
                coefficients. yh is a list of length J with the first entry
                being the finest scale coefficients. yl has shape
                :math:`(N, C_{in}, H_{in}', W_{in}')` and yh has shape
                :math:`list(N, C_{in}, 3, H_{in}'', W_{in}'')`. The new
                dimension in yh iterates over the LH, HL and HH coefficients.
        Note:
            :math:`H_{in}', W_{in}', H_{in}'', W_{in}''` denote the correctly
            downsampled shapes of the DWT pyramid.
        """
        yh = []
        ll = x
        mode = lowlevel.mode_to_int(self.mode)

        # Do a multilevel transform
        for j in range(self.J):
            # Do 1 level of the transform
            ipdb.set_trace()
            ll, high = lowlevel.my_synthesize(
                ll, self.h0_col, self.h1_col, self.h0_row, self.h1_row, 0)
            yh.append(high)

        return ll, yh
    
    def forward(self, x, type='s'):
        if type=='s':
            return self.do_synthesize(x)
        if type=='r':
            return self.do_reconstruct(x)


mat = scipy.io.loadmat('boat.mat')
X = mat['x']
X = torch.tensor(X).unsqueeze(0).unsqueeze(0).float()

mydwt  = DWT2D(J=1)
my_Yl, my_Yh = mydwt(X,'s')
#my_Yh[0][my_Yh[0]<0.00000001]=0
Y = mydwt((my_Yl, my_Yh),'r')

mydwt.theta1.retain_grad()
my_Yl.mean().backward()
print(mydwt.theta1.grad)
print( ((X-Y)**2).mean())